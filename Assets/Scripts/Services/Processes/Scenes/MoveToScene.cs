using System;

public class MoveToScene : Process
{
    private readonly SceneName _scene;
    private readonly Action<SceneName> _loadSceneCallback;

    public MoveToScene(SceneName scene, Action<SceneName> LoadSceneCallback)
    {
        _scene = scene;
        _loadSceneCallback = LoadSceneCallback;
    }

    protected override bool OnRun()
    {
        Add(new UnloadCurrentScene());
        Add(new LoadScene(_scene, _loadSceneCallback));
        
        return true;
    }
}