using System;

public class WaitGameInitialization : Process
{
    private readonly Action _actionIsRequired;

    public WaitGameInitialization(Action actionIsRequired)
    {
        _actionIsRequired = actionIsRequired;
    }

    protected override bool OnRun()
    {
        _actionIsRequired?.Invoke();

        return true;
    }

    protected override void OnComplete()
    {
        base.OnComplete();
    }
}
