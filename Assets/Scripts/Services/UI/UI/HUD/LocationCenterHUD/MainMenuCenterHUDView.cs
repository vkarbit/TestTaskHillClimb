using UI;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuCenterHUDView : HudView
{
    [SerializeField] private Button _startGameButton;
    public Button StartGameButton => _startGameButton;
}
