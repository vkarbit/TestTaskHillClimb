using Base;
using UI;

public class MainMenuHUDMediator : HudMediator<MainMenuCenterHUDView>
{
    protected override void OnMediate()
    {
        base.OnMediate();

        View.StartGameButton.onClick.AddListener(StartGameButtonOnClick);
    }

    protected override void OnUnMediate()
    {
        base.OnUnMediate();

        View.StartGameButton.onClick.RemoveListener(StartGameButtonOnClick);
    }

    private void StartGameButtonOnClick()
    {
        Services.GameState.RunTo(GameState.Game);
    }
}
