using Context.Services.InputService;
using UI;
using UnityEngine;

public class GameBottomHUDView : HudView
{
    [SerializeField] private ButtonInput _backButton;
    [SerializeField] private ButtonInput _forwardButton;

    private InputService _inputService;

    private void Update()
    {
        float buttonInput = 0f;

        if (_forwardButton.IsPressed())
        {
            buttonInput = 1f;
        }
        else if (_backButton.IsPressed())
        {
            buttonInput = -1f;
        }

        _inputService.SetForward(buttonInput);
    }

    public void GetInputService(InputService inputService)
    {
        _inputService = inputService;
    }
}
