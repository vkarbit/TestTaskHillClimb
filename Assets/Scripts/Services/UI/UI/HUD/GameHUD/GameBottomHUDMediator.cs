using UI;

public class GameBottomHUDMediator : HudMediator<GameBottomHUDView>
{
    protected override void OnMediate()
    {
        base.OnMediate();

        View.GetInputService(Services.InputService);
    }
}
