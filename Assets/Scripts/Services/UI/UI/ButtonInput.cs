using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonInput : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private bool isPressed = false; // ���� ��� ������������ ������� ������

    public bool IsPressed()
    {
        return isPressed;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isPressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPressed = false;
    }
}