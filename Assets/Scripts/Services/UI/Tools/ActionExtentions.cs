public static class OneListenerExtentions
{
    public static void SafeInvoke(this OneListener invocationTarget)
    {
        if (null != invocationTarget)
        {
            invocationTarget.Invoke();
        }
    }

    public static void SafeInvoke<T>(this OneListener<T> invocationTarget, T arg)
    {
        if (null != invocationTarget)
        {
            invocationTarget.Invoke(arg);
        }
    }

    public static void SafeInvoke<T1, T2>(this OneListener<T1, T2> invocationTarget, T1 arg1, T2 arg2)
    {
        if (null != invocationTarget)
        {
            invocationTarget.Invoke(arg1, arg2);
        }
    }

}