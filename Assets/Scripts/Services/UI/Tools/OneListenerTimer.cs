using System;

public class OneListenerTimer : BaseOneListener<Action<int>>
{
    public OneListenerTimer()
    {
        Value = 1;
    }

    public int Value { get; private set; }

    protected override void OnAdd(Action<int> action)
    {
        base.OnAdd(action);
        
        action?.Invoke(Value);
    }

    public void Invoke(int value)
    {
        Value = value;
        
        if (_count == 0)
            return;

        int length = _list.Count;
        for (int i = 0; i < Math.Min(length, _list.Count); i++)
        {
            var current = _list[i];
            if (current != null)
            {
                current.Invoke(value);
            }
        }

        if (_count == _list.Count)
            return;

        for (int i = _list.Count - 1; i >= 0; i--)
        {
            if (null == _list[i])
            {
                _list.RemoveAt(i);
            }
        }
    }
}