using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI.Configuration
{
    public class ConfigurationView : MonoBehaviour
    {
        [SerializeField] private Button _playMode;
        [SerializeField] private Button _editorMode;

        private void Awake()
        {
            _playMode.onClick.AddListener(StartGame);
            _editorMode.onClick.AddListener(StartEditor);
        }

        private void Start()
        {

            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 300;

#if !UNITY_EDITOR
{
StartGame();
}
#endif
        }

        private void OnEnable()
        {
            _playMode.image.color = new Color32(145,231,148,255);
            _editorMode.image.color = new Color32(137, 126, 255, 255);
        }

        private void StartEditor()
        {
            Defines.SetEditorMode();
            StartGame();
        }

        private void StartGame()
        {
            SceneManager.LoadScene("Scenes/Entrance");
        }
    }
}