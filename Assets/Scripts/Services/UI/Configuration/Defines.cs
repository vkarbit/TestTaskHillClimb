public static class Defines
{
    public static bool EditorMode { get; private set; }

    public static void SetEditorMode()
    {
        EditorMode = true;
    }
}