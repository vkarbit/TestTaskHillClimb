namespace UI
{
    public abstract class SplashScreenMediator : BaseMediator
    {
        public SplashScreenName Name { get; private set; }

        public void SetName(SplashScreenName name)
        {
            Name = name;
        }
    }

    public abstract class SplashScreenMediator<T> : SplashScreenMediator
        where T : BaseView
    {
        protected T View { get; private set; }

        protected sealed override void OnSetView(BaseView view)
        {
            View = view as T;
        }

        protected override void OnSetData(BaseData data)
        {
        }

        protected sealed override void Mediate()
        {
            OnMediate();
        }

        protected virtual void OnMediate()
        {
        }

        public sealed override void Show()
        {
            View.Show();
            OnShow();
        }

        protected virtual void OnShow()
        {
        }

        public sealed override void Hide()
        {
            if (View != null) View.Hide();
            OnHide();
        }

        protected virtual void OnHide()
        {
        }

        public sealed override void UnMediate()
        {
            OnUnMediate();
        }

        protected virtual void OnUnMediate()
        {
        }
    }
}