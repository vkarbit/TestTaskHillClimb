public enum SceneName
{
    Entrance = 1,
    SplashScreen = 2,
    MainMenu = 3,
    Game = 4
}