﻿using Base;


namespace Context.Services.InputService
{
    public class InputService : Service
    {
        private float _moveInput;
        public void SetForward(float input)
        {
            _moveInput = input;
        }

        public float GetForward()
        {
            return _moveInput;
        }
    }
}