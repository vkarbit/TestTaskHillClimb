using UI;

namespace UIMapper
{
    public class SplashScreenMapper : UIMapper<SplashScreenName, SplashScreenMediator, SplashScreenView>
    {
        public SplashScreenMapper()
        {
            Map<DefaultSplashScreenMediator>(SplashScreenName.Default);
            Map<StartSplashScreenMediator>(SplashScreenName.Start);
        }
    }
}