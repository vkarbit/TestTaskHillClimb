using Processes.SplashScreen;

namespace Base.GameStates
{
    public class EntranceToLocation : GameStateTransition
    {
        protected override void OnStart()
        {
            base.OnStart();

            Add(new ShowSplashScreen(SplashScreenName.Start, SplashScreen));
        }

        protected override void OnEnd()
        {
            base.OnEnd();

            Add(new MoveToScene(SceneName.MainMenu, OnSceneLoaded));
            Add(new HideSplashScreen(SplashScreen));
        }
    }
}