namespace Base.GameStates
{
    public class InitializeToEntrance : GameStateTransition
    {
        protected override void OnStart()
        {
            base.OnStart();

            //OnSceneLoaded(SceneName.Entrance);
            Add(new LoadScene(SceneName.SplashScreen, null));
        }

        protected override void OnCompleteAll()
        {
            base.OnCompleteAll();

            Services.GameState.RunTo(GameState.MainMenu);
        }
    }
}