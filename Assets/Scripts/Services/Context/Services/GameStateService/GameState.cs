using System;

namespace Base
{
    [Serializable]
    public class GameState : State
    {
        public static readonly GameState Initialize = new GameState("INITIALIZE", 0);
        public static readonly GameState Entrance = new GameState("ENTRANCE", 1);
        public static readonly GameState MainMenu = new GameState("MAINMENU", 2);
        public static readonly GameState Game = new GameState("GAME", 3);

        public GameState(string name, int value, State parent = null) : base(name, value, parent)
        {
        }
    }
}