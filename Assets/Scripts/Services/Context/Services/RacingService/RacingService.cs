using Base;

public class RacingService : Service
{
    protected override void BindHandlers()
    {
        base.BindHandlers();

        Handlers.Add(_ = new RacingInitializerHandler());
    }
}
