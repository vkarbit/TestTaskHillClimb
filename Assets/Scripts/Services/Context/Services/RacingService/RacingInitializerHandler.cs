using Base;
using Cinemachine;
using UnityEngine;

public class RacingInitializerHandler : Handler
{
    protected override void OnInitialize()
    {
        base.OnInitialize();

        Services.GameState.TransitionRequiredAction += TryInitializeRacing;
    }

    private void TryInitializeRacing(GameState state)
    {
        if (state == GameState.Game)
        {
            InitializeRacing();
        }
    }

    private void InitializeRacing()
    {
        CarContorller car = Services.Resources.Prefabs.GetInstance<CarContorller>(PrefabName.Car);

        car.Init(Services.InputService);

        CinemachineVirtualCamera cam = GameObject.FindGameObjectWithTag("CinemachineCamera")
            .GetComponent<CinemachineVirtualCamera>();

        cam.Follow = car.transform;

        
    }
}
