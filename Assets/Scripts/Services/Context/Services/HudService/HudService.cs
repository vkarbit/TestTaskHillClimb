using System.Collections.Generic;
using Base.HUD;
using UI;
using UnityEngine;

namespace Base
{
    public class HudService : Service
    {
        private HUDProvider _hudProvider;
        private Dictionary<HUDName, HudMediator> _cache;
        private Queue<HudMediator> _actives;

        protected override void OnInitialize()
        {
            base.OnInitialize();
            
            _hudProvider = new HUDProvider(Services);
            _cache = new Dictionary<HUDName, HudMediator>();
            _actives = new Queue<HudMediator>();

            Services.GameState.TransitionStart += OnStateChange;
        }

        private void OnStateChange(GameState state)
        {
            TryShow(state);
        }

        private void TryShow()
        {
            TryShow(Services.GameState.Current);
        }

        private void TryShow(GameState state)
        {
            HideAll();
            
            if (state == GameState.MainMenu)
            {
                Show(HUDName.MainMenuCenterHUD);
                return;
            }
            
            if (state == GameState.Game)
            {
                Show(HUDName.GameBottomHUD);
                return;
            }
        }

        private void Show(HUDName name, HudData data = null)
        {
            HudMediator hud;

            if (_cache.ContainsKey(name))
            {
                hud = _cache[name];
            }
            else
            {
                hud = _hudProvider.Get(name);

                if (hud == null)
                {
                    Debug.LogError("no windowType ");
                    return;
                }

                _cache.Add(name, hud);
            }

            hud.SetData(data);
            hud.Show();
            
            _actives.Enqueue(hud);
        }

        private void HideAll()
        {
            while (_actives.Count > 0)
            {
                Hide(_actives.Dequeue().Name);
            }
        }

        public void Hide(HUDName hudName)
        {
            if (_cache.ContainsKey(hudName))
            {
                _cache[hudName].Hide();
            }
        }

        protected override void OnDispose()
        {
            base.OnDispose();
            
            Services.GameState.TransitionStart -= OnStateChange;
        }
    }
}