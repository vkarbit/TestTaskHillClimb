using UI;

public enum HUDName
{
    None = 0,
    MainMenuCenterHUD = 1,
    GameBottomHUD = 2,
}

namespace UIMapper
{
    public class HudMapper : UIMapper<HUDName, HudMediator, HudView>
    {
        public HudMapper()
        {
            Map<MainMenuHUDMediator>(HUDName.MainMenuCenterHUD);
            Map<GameBottomHUDMediator>(HUDName.GameBottomHUD);
        }
    }
}