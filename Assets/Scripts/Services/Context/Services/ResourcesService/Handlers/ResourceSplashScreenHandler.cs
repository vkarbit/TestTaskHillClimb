using UnityEngine;

namespace Base.Handlers
{
    public class ResourceSplashScreenHandler : ResourcePrefabsHandler
    {
        protected override string Path => "SplashScreen";
        
        public bool TryGetInstance(SplashScreenName prefabName, out GameObject go)
        {
            var prefab = GetInstance(prefabName.ToString());
            if (prefab == null)
            {
                go = null;
                return false;
            }

            go = prefab;
            return true;
        }
    }
}