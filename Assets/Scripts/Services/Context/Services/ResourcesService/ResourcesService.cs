using Base.Handlers;

namespace Base
{
    public class ResourcesService : Service
    {
        public ResourceSpritesHandler Sprites { get; private set; }
        public ResourcePrefabsHandler Prefabs { get; private set; }
        public ResourcesHudHandler HUD { get; private set; }
        public ResourceSplashScreenHandler SplashScreen { get; set; }
        public ResourceScriptableObjectsHandler ScriptableObjects { get; set; }

        protected override void BindHandlers()
        {
            base.BindHandlers();
            
            Handlers.Add(Sprites = new ResourceSpritesHandler());
            Handlers.Add(Prefabs = new ResourcePrefabsHandler());
            Handlers.Add(HUD = new ResourcesHudHandler());
            Handlers.Add(SplashScreen = new ResourceSplashScreenHandler());
            Handlers.Add(ScriptableObjects = new ResourceScriptableObjectsHandler());
        }
    }
}