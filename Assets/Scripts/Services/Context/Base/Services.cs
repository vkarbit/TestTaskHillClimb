using System.Collections.Generic;
using Context.Services.InputService;


namespace Base
{
    public class Services : ServiceInitializer
    {
        private readonly List<Service> _services;
        private ServiceInitializer _initializer;

        public ResourcesService Resources { get; }
        public GameStateService GameState { get; }
        public ScenesService Scene { get; }
        public ProcessesService Processes { get; }
        public UILayersService UILayers { get; }
        public HudService HUD { get; }
        public InputService InputService { get; }
        public UILinkService UILink { get; }
        public RacingService Racing { get; set; }


        public Services()
        {
            _services = new List<Service>();
            

            Add(Resources = new ResourcesService());
            Add(GameState = new GameStateService());
            Add(Scene = new ScenesService());
            Add(Processes = new ProcessesService());
            Add(UILayers = new UILayersService());
            Add(HUD = new HudService());
            Add(InputService = new InputService());
            Add(UILink = new UILinkService());
            Add(Racing = new RacingService());
        }

        private void Add(Service service)
        {
            _services.Add(service);
        }

        public void Run()
        {
            _initializer = new ServiceInitializer();

            foreach (var service in _services)
            {
                service.InitializeService(_initializer);
            }
            
            _initializer.Run(this);
        }

        public void Dispose()
        {
            foreach (var service in _services)
            {
                service.Dispose();
            }
        }
    }
}