using Base;
using UnityEngine;

public abstract class ServicesInjectorBehaviour : MonoBehaviour
{
    protected Services Services { get; private set; }
    
    public void Inject(Services services)
    {
        if (Services != null) return;
        
        Services = services;
        OnInitialize();
    }
    
    protected virtual void OnInitialize() { }
}