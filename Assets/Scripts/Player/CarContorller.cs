using Context.Services.InputService;
using UnityEngine;

public class CarContorller : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _frontTireRb;
    [SerializeField] private Rigidbody2D _backTireRb;

    [SerializeField] private Rigidbody2D _carRb;

    [SerializeField] private float _speed = 150f;
    [SerializeField] private float _rotationSpeed = 300f;

    private float _moveInput;

    private InputService _inputService;

    public void Init(InputService inputService)
    {
        _inputService = inputService;
    }

    private void Update()
    {
        _moveInput = _inputService.GetForward();
    }

    private void FixedUpdate()
    {
        _frontTireRb.AddTorque(-_moveInput * _speed * Time.fixedDeltaTime);
        _backTireRb.AddTorque(-_moveInput * _speed * Time.fixedDeltaTime);
        _carRb.AddTorque(_moveInput * _rotationSpeed * Time.fixedDeltaTime);
    }
}
